const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', updateOutputField);

txtLastName.addEventListener('keyup', updateOutputField);

function updateOutputField(e) {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}